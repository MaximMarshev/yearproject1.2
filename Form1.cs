﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Year_project1._2
{
    public partial class Form1 : Form
    {
        Graph graph;
        Bitmap k;
        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            graph = new Graph(openFileDialog1.FileName);
                            List<int> arr = new List<int>();
                            k = graph.Draw(pictureBox1.Width, pictureBox1.Height);
                            pictureBox1.Image = k;
                            var bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DFS_Click(object sender, EventArgs e)
        {
            int V;
            if (!int.TryParse(textBoxV.Text, out V))
            {
                textBoxV.Text = "Введите вершину";
                return;
            }
            if (V < 1 || V > graph.nCount)
            {
                textBoxV.Text = "V>0 && V<=" + graph.nCount.ToString();
                return;
            }
            //
            //
            //
            graph.DFS(V - 1);
            //
            //
            //
            int i;
            List<bool> used = new List<bool>();
            for ( i = 0; i < graph.nCount; ++i)
                used.Add(false);
            //
            //
            //

            
            var graphics = Graphics.FromImage(k);
            var timer = new Timer();
            timer.Interval = 2000;
            i = 0;
            timer.Start();
            timer.Tick += new EventHandler((o, ev) =>
            {
                var BlackPen = new Pen(Color.Black);
                var RedPen = new Pen(Color.Red);
                BlackPen.Width = 5;
                RedPen.Width = 3;
                var fon = new SolidBrush(Color.White);
                if(!used[graph.array[i] - 1])
                 {
                    graphics.DrawEllipse(BlackPen, graph.Сoordinates[graph.array[i] - 1].X - graph.r, graph.Сoordinates[graph.array[i] - 1].Y - graph.r, 2 * graph.r, 2 * graph.r);
                    graphics.DrawLine(RedPen, graph.Сoordinates[graph.array[i] - 1].X, graph.Сoordinates[graph.array[i] - 1].Y, graph.Сoordinates[graph.array[i+1]-1].X, graph.Сoordinates[graph.array[i+1] - 1].Y);
                    pictureBox1.Refresh();
                    used[graph.array[i] - 1] = true;
                 }
                 else
                 {
                     // показываем, что не можем туда перейти
                 }
                ++i;
                if (i >= graph.array.Count)
                    timer.Stop();
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = k = graph.Draw(pictureBox1.Width, pictureBox1.Height);
        }
    }
}
