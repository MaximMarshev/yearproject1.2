﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Year_project1._2
{
    class Graph
    {
        public int nCount = 0;     //количество вершин в графе 
        public int mCount = 0;     //колличество ребер в графе
        public int r;              //радиус Вершины
        bool flag = true;
        List<int>[] graph;  //список смежности
        List<bool> used;    //массив для хранения информации о пройденных и не пройденных вершинах
        public List<Point> Сoordinates; //координты x и y
        public List<int> array; //массив проойденных вершин

        //Конструктор
        public Graph(string FileName)
        {
            FileStream fin;
            string str;
            int m = 0;
            try
            {
                fin = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                StreamReader fin_in = new StreamReader(fin);


                if (!fin_in.EndOfStream)
                {
                    str = fin_in.ReadLine();
                    this.nCount = Convert.ToInt32(str);
                    this.graph = new List<int>[this.nCount];
                    for (int i = 0; i < this.nCount; ++i)
                        this.graph[i] = new List<int>();

                    while (!fin_in.EndOfStream)
                    {
                        str = fin_in.ReadLine();
                        Char delimiter = ' ';
                        string[] substrings = str.Split(delimiter);
                        if (2 != substrings.Length)
                            return;
                        int v = Convert.ToInt32(substrings[0]) - 1;
                        int w = Convert.ToInt32(substrings[1]) - 1;
                        this.graph[v].Add(w);
                        this.graph[w].Add(v);
                        ++m;
                    }
                }
                fin_in.Close();
            }
            catch (IOException exc)
            {
                //Сдесь должен быть MessageBox
                return;
            }
            fin.Close();
            mCount = m;
        }

        //Рисование графа 
        public Bitmap Draw(int width, int height)
        {
            if (flag)
            {
                var CenterX = width / 2 + 40;
                var CenterY = height / 6;
                var R = Math.Min(width, height) / 5;             //радиус большого круга
                r = Math.Min(width, height) / this.nCount / 2; //радиус маленького круга
                var alpha = (2 * Math.PI) / this.nCount;         //Угол 
                this.Сoordinates = new List<Point>();
                this.Сoordinates.Add(new Point(CenterX, CenterY));
                //
                //Вычисляем координты
                //

                for (int i = 0; i < this.nCount - 1; ++i)
                {
                    CenterX = (int)(CenterX + (R * Math.Cos(alpha)));
                    CenterY = (int)(CenterY + (R * Math.Sin(alpha)));
                    this.Сoordinates.Add(new Point(CenterX, CenterY));
                    alpha += (2 * Math.PI) / this.nCount;
                }
                flag = false;
            }
            //
            //
            //
            var bitmap = new Bitmap(width, height);
            var graphics = Graphics.FromImage(bitmap);
            var GreenPen = new Pen(Color.Green);
            GreenPen.Width = 2;
            //
            //
            //Рисуем ребра
            for (int i = 0; i < this.nCount; ++i)
                for (int j = 0; j < graph[i].Count; ++j)
                {
                    graphics.DrawLine(GreenPen, this.Сoordinates[i].X, this.Сoordinates[i].Y, this.Сoordinates[graph[i][j]].X, this.Сoordinates[graph[i][j]].Y);
                }
            //
            //
            //
            var BlackPen = new Pen(Color.Black);
            BlackPen.Width = 2;
            var fon = new SolidBrush(Color.White);
            //
            //
            //Рисуем Вершины
            for (int i = 0; i < this.nCount; ++i)
            {
                graphics.FillEllipse(fon, this.Сoordinates[i].X - r, this.Сoordinates[i].Y - r, r * 2, r * 2);
                graphics.DrawEllipse(BlackPen, this.Сoordinates[i].X-r, this.Сoordinates[i].Y-r, 2 * r, 2 * r);
            }
            //
            //
            //
            Font font = new Font("Arial", 15);
            Brush brush = Brushes.Black;
            int number = 1;
            //
            //
            //Рисуем Цифры
            for(int i = 0;i< this.nCount;++i)
            {
                graphics.DrawString(number.ToString(), font, brush, this.Сoordinates[i].X - r/2, this.Сoordinates[i].Y - r/2);
                number += 1;
            }
            return bitmap;
        }



        void Search(int V)
        {
            //если вершина является пройденной, то не производим из нее вызов процедуры
            if (this.used[V])
            {
                this.array.Add(V+1);
                return;
            }
            this.used[V] = true; //помечаем вершину как пройденную
            this.array.Add(V+1);
            //запускаем обход из всех вершин, смежных с вершиной v    
            for (int i = 0; i < this.graph[V].Count; ++i)
            {
                int w = this.graph[V][i];
                Search(w); //вызов обхода в глубину от вершины w, смежной с вершиной v
            }
        }

        public void DFS(int V)
        {
            this.used = new List<bool>();
            for (int i = 0; i < this.nCount; ++i)
                this.used.Add(false);
            this.array = new List<int>();
            for(int i = V; i < this.nCount;++i)
            {
                Search(i);
            }
            for (int i = 0; i < V; ++i)
            {
                Search(i);
            }
        }
    }
}
